package it.b0sh.socialImporter.utils;


import org.slf4j.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


public abstract class Configurer {

	private static final Logger log = LoggerFactory.getLogger( Configurer.class );

	private Map<String,String> properties;

	private Properties allProperties;

	private boolean loaded = false;


	protected Configurer(){
	}

	private void dynaLoad() throws Exception{

		properties = new HashMap<String,String>();


		try{
			//Caricamento dinamico di tutte le proprieta' definite come "public final static"
			Field[] fields = getClass().getFields();
			int mask = Modifier.FINAL | Modifier.STATIC | Modifier.PUBLIC;
			for( Field field : fields ){
				if( Property.class.isAssignableFrom( field.getType() ) && ( field.getModifiers() & mask ) == mask ){
					String key;
					try{
						key = ( (Property)field.get( null ) ).key();
					}catch( IllegalAccessException e ){
						//Non dovrebbe mai verificarsi in quanto si sta cercando di accedere
						//ad un campo della classe stessa
						throw new AssertionError();
					}
					String value = allProperties.getProperty( key );
					if( value == null )
						log.warn( "Can't find a value for property: " + key );
					properties.put( key, value );
				}
			}
			loaded = true;
		}catch( Exception e ){
			throw new Exception( "Configuration initialization failed.", e );
		}

	}

	public final void load(File... files) throws Exception{
		allProperties = new Properties();

		for( File file : files ){
			try{
				allProperties.load( new FileInputStream(file));
			}catch( Exception e ){
				throw new Exception( "Configuration initialization failed. Error reading from properties file: " , e );
			}
		}

		dynaLoad();



	}

	/**
	 * Dato un <code>URL</code> rappresentante una risorsa riconoscibile dall'oggetto
	 * <code>Properties</code>, valorizza tutte le costanti definite nella classe
	 * con i corrispettivi valori ricercati nel file properties.
	 */
	public final void load( URL... urls ) throws Exception {
		allProperties = new Properties();

		for( URL url : urls ){
			try{
				allProperties.load( url.openStream() );
			}catch( Exception e ){
				throw new Exception( "Configuration initialization failed. Error reading from properties file: " + url.getFile(), e );
			}
		}

		dynaLoad();


	}

	public final Property getProperty( String name ){
		return new Property( name );
	}

	public final void reset(){
		properties = null;
	}


	public final class Property{

		private final String key;

		public Property( String key ){
			this.key = key;
		}

		public final String value(){
			if( properties == null )
				throw new RuntimeException( "Configurer class not initialized. Missing call to load()" );
			String result = properties.get( key );
			if( result == null )
				result = (String)allProperties.get( key );
			return result;
		}

		public final int intValue(){
			return Integer.parseInt(value());
		}

		public final long longValue(){
			return Long.parseLong(value());
		}

		public final boolean booleanValue(){
			String value = value();
			if( value == null ) return false;
			value = value.toLowerCase();
			return "true".equals( value ) || "y".equals( value ) || "yes".equals( value );
		}

		public final String key(){
			return key;
		}
	}

	public boolean isLoaded(){
		return loaded;
	}

}
