package it.b0sh.delicious;

import java.io.File;
import java.net.URL;

/**
 * @author Simone Galliani
 *         Date: 12/04/2015
 *         Time: 18:58
 *         Versioni:
 *         1.0 - 12/04/2015 - SG Creazione del file
 */
public class Main {

    public static void main(String[] args){

        DeliciousImporter importer = new DeliciousImporter( new File("delicious-config.properties"));
        importer.doImport();

    }

}
