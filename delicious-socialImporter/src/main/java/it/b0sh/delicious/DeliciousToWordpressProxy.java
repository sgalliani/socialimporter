package it.b0sh.delicious;

import com.tearsofaunicorn.wordpress.api.model.Post;
import com.tearsofaunicorn.wordpress.api.model.Tag;
import it.b0sh.delicious.model.DeliciousPost;

/**
 * @author Simone Galliani
 *         Date: 14/04/2015
 *         Time: 21:15
 *         Versioni:
 *         1.0 - 14/04/2015 - SG Creazione del file
 */
public class DeliciousToWordpressProxy {

    private DeliciousPost post;

    public DeliciousToWordpressProxy(DeliciousPost post) {
        this.post = post;
    }

    public Post getPost() {
        Post wpost = new Post(Config.WORDPRESS_TITLE.value().replace("${description}", post.getDescription()),
                Config.WORDPRESS_TEXT.value()
                        .replace("${name}", post.getName())
                        .replace("${url}", post.getUrl()));

        for(String tag : post.getTags())
            wpost.addTag(new Tag(tag));



        return wpost;
    }
}
