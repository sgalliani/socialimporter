package it.b0sh.delicious;

import it.b0sh.socialImporter.utils.Configurer;

import java.io.File;
import java.net.URL;


public class Config extends Configurer {
	
	protected static final Configurer config = new Config();
	
	public final static Configurer.Property USERNAME               			= config.new Property( "username" );
	public final static Property URL		               			= config.new Property( "url" );
	public final static Property WORDPRESS_TITLE           			= config.new Property( "wordpress.title" );
	public final static Property WORDPRESS_TEXT	           			= config.new Property( "wordpress.text" );
	public final static Property WORDPRESS_AUTHOR          			= config.new Property( "wordpress.author" );
	public final static Property WORDPRESS_USERNAME        			= config.new Property( "wordpress.username" );
	public final static Property WORDPRESS_PASSWORD        			= config.new Property( "wordpress.password" );
	public final static Property WORDPRESS_URL          			= config.new Property( "wordpress.url" );
	public final static Property OLDPOSTS_ARCHIVE_FILENAME			= config.new Property( "oldpostarchive.filename" );

	protected Config(){
	}

	public static void init( URL... urls ) throws Exception {
		config.load( urls );
	}

	public static void init( File... files ) throws Exception {
		config.load( files );
	}

	public static void unload(){
		config.reset();
	}

}
