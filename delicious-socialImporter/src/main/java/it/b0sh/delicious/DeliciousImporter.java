package it.b0sh.delicious;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tearsofaunicorn.wordpress.api.WordpressClient;
import com.tearsofaunicorn.wordpress.api.WordpressClientConfig;
import com.tearsofaunicorn.wordpress.api.WordpressClientException;
import com.tearsofaunicorn.wordpress.api.model.Post;
import com.tearsofaunicorn.wordpress.api.transport.XmlRpcBridge;
import it.b0sh.delicious.model.DeliciousPost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Simone Galliani
 *         Date: 12/04/2015
 *         Time: 22:26
 *         Versioni:
 *         1.0 - 12/04/2015 - SG Creazione del file
 */
public class DeliciousImporter {

    private static final Logger log = LoggerFactory.getLogger(DeliciousImporter.class);

    private List<DeliciousPost> oldPost;

    public DeliciousImporter(URL config) {
        try {
            Config.init(config);
        } catch (Exception e) {
            throw new RuntimeException("Configuration not  laoded", e);
        }
    }

    public DeliciousImporter(File config) {
        try {
            Config.init(config);
        } catch (Exception e) {
            throw new RuntimeException("Configuration not  laoded", e);
        }
    }

    public void doImport(){


        Gson gson = new Gson();
        Type listPosts = new TypeToken<Collection<DeliciousPost>>() {}.getType();


        try {

            File oldPostFile = new File( Config.OLDPOSTS_ARCHIVE_FILENAME.value() );
            if (oldPostFile.exists()) {
                oldPost = gson.fromJson(new FileReader(oldPostFile),listPosts);
            } else {
                oldPost = new ArrayList<DeliciousPost>();
            }

            URL url = new URL(Config.URL.value().replace("${username}", Config.USERNAME.value()));
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(15000);
            connection.setReadTimeout(15000);

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));

            String line;
            StringBuilder sb = new StringBuilder();
            while (  (line = in.readLine()) != null) {
                log.debug(line);
                sb.append(line);
            }
            log.info("\nREST Service Invoked Successfully..");
            in.close();

            Collection<DeliciousPost> deliciousPosts = gson.fromJson(sb.toString().replace("\"t\": \"\"", "\"t\": [\"\"]"),listPosts);


            WordpressClientConfig config;
            try {
                config = new WordpressClientConfig(Config.WORDPRESS_USERNAME.value(),
                        Config.WORDPRESS_PASSWORD.value(),
                        new URL(Config.WORDPRESS_URL.value()));
            } catch (MalformedURLException e) {
                throw new WordpressClientException("Failed to build Wordpress XML-RPC URL using configured property", e);
            }

            WordpressClient wpClient= new WordpressClient(new XmlRpcBridge(config));


            for (DeliciousPost post: deliciousPosts ){
                if (!oldPost.contains(post)) {
                    Post wpost = (new DeliciousToWordpressProxy(post)).getPost();
                    log.debug(wpost.toString());
                    log.info("Created new post, id : " + wpClient.newPost(wpost));
                }
            }

            oldPostFile.delete();
            oldPostFile.createNewFile();

            FileWriter fw = new FileWriter(oldPostFile);

            gson.toJson(deliciousPosts,fw);

            fw.flush();
            fw.close();





        } catch (MalformedURLException u) {
            log.warn("Not valid url or username");
            throw new RuntimeException("Not valid url or username",u);
        } catch (IOException i){
            log.warn("Impossible to read data");
            throw new RuntimeException("Impossible to read data",i);
        }

    }
}
