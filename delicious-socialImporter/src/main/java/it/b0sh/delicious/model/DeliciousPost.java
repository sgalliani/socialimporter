package it.b0sh.delicious.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Simone Galliani
 *         Date: 12/04/2015
 *         Time: 22:40
 *         Versioni:
 *         1.0 - 12/04/2015 - SG Creazione del file
 */
public class DeliciousPost implements Serializable {

    @SerializedName("a")
    private String author;
    @SerializedName("d")
    private String description;
    @SerializedName("n")
    private String name;
    @SerializedName("u")
    private String url;
    @SerializedName("t")
    private List<String> tags;
    @SerializedName("dt")
    private Date date;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public void setTags(String tags){
        ;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "\nPost{\n\t" +
                "author='" + author + '\'' +
                ",\n\t description='" + description + '\'' +
                ",\n\t name='" + name + '\'' +
                ",\n\t url='" + url + '\'' +
                ",\n\t tags=" + tags +
                ",\n\t date=" + date +
                "\n\ttoString()=" + super.toString() + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeliciousPost that = (DeliciousPost) o;

        if (url != null ? !url.equals(that.url) : that.url != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return url != null ? url.hashCode() : 0;
    }
}
