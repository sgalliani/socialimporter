package it.b0sh.instagram;

import java.io.File;

/**
 * Created by Simone on 29/09/2015.
 */
public class LocalMover implements IMover {

    private String directory;

    public LocalMover(String directory) {
        this.directory = directory;
    }

    public void move(File file, String destFilename) {
        if (file.renameTo(new File(directory + destFilename))) {
            ;
        } else {
            throw new RuntimeException("Move to " + (directory + destFilename) + " failed");
        }
    }

    public void detach() {
        ;
    }
}
