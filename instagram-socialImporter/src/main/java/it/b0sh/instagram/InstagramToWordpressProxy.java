package it.b0sh.instagram;


import com.tearsofaunicorn.wordpress.api.model.Post;
import com.tearsofaunicorn.wordpress.api.model.Tag;
import org.jinstagram.entity.users.feed.MediaFeedData;

/**
 * @author Simone Galliani
 *         Date: 18/06/2015
 *         Time: 21:39
 *         Versioni:
 *         1.0 - 18/06/2015 - SG Creazione del file
 */
public class InstagramToWordpressProxy {

    private MediaFeedData instagramMedia;

    public InstagramToWordpressProxy(MediaFeedData instagramMedia) {
        this.instagramMedia = instagramMedia;
    }

    public Post getPost(){

        String title = instagramMedia.getCaption()!=null ? instagramMedia.getCaption().getText() : "Foto";

        Post wpost = new Post(Config.WORDPRESS_TITLE.value().replace("${caption}", title),
                Config.WORDPRESS_TEXT.value().replace("${url}", Config.HTTP_BASE_PATH.value() + instagramMedia.getId() + ".jpg"));

        for(String tag : instagramMedia.getTags())
            wpost.addTag(new Tag(tag));



        return wpost;

    }
}
