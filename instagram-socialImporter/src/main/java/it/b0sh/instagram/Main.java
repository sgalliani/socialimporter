package it.b0sh.instagram;

/**
 * @author Simone Galliani
 *         Date: 30/05/2015
 *         Time: 16:15
 *         Versioni:
 *         1.0 - 30/05/2015 - SG Creazione del file
 */
public class Main {

    public static void main(String[] args) {

        InstagramImporter importer = new InstagramImporter(Main.class.getResource("/config.properties"));
        importer.doImport();

    }

}