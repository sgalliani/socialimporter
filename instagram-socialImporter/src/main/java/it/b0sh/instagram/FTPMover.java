package it.b0sh.instagram;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Simone on 29/09/2015.
 */
public class FTPMover implements IMover {

    private FTPClient ftpClient;
    private FTPClientConfig config;
    private String directory;


    private static final Logger log = LoggerFactory.getLogger(FTPMover.class);



    public FTPMover(String ftpServer, String username, String password, String directory) {

        FTPClient ftpClient = new FTPClient();
        FTPClientConfig config = new FTPClientConfig();
        ftpClient.configure(config);

        this.directory = directory;

        try {

            ftpClient.connect(ftpServer);

            if (!FTPReply.isPositiveCompletion(ftpClient.getReplyCode())) {
                log.error("Can't connect to FTP Server");
                throw new RuntimeException("Can't connect to FTP Server");
            }


            if (!ftpClient.login(username, password)) {
                log.error("Can't login to FTP Server.");
                throw new RuntimeException("Can't login to FTP Server.");
            }

            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

        } catch (IOException ioe) {
            log.error("Can't start FTP session: " + ioe.getMessage());
            throw new RuntimeException(ioe);
        }

    }

    public void move(File file, String destFilename) {

        try {
            FileInputStream input = new FileInputStream(file);
            boolean result = ftpClient.storeFile(directory + destFilename + ".jpg", input);
            input.close();
            if (!result)
                throw new RuntimeException("Can't store file on remote server");

        } catch (FileNotFoundException fnfe) {
            throw new RuntimeException(fnfe);
        } catch (IOException ioe){
            throw new RuntimeException(ioe);
        }

    }

    public void detach() {
        if(ftpClient.isConnected()) {
            try {
                ftpClient.disconnect();
            } catch(IOException ioe) {
                // do nothing
            }
        }
    }
}
