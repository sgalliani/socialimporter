package it.b0sh.instagram;

import java.io.File;

/**
 * Created by Simone on 29/09/2015.
 */
public interface IMover {

    public void move(File file, String destFilename);

    public void detach();

}
