package it.b0sh.instagram;

import it.b0sh.socialImporter.utils.Configurer;

import java.net.URL;


public class Config extends Configurer {
	
	protected static final Configurer config = new Config();
	
	public final static Property USERNAME               			= config.new Property( "username" );
    public final static Property CLIENT_ID	               			= config.new Property( "client.id" );
    public final static Property CLIENT_SECRET             			= config.new Property( "client.secret" );
	public final static Property WORDPRESS_TITLE           			= config.new Property( "wordpress.title" );
	public final static Property WORDPRESS_TEXT	           			= config.new Property( "wordpress.text" );
	public final static Property WORDPRESS_AUTHOR          			= config.new Property( "wordpress.author" );
	public final static Property WORDPRESS_USERNAME        			= config.new Property( "wordpress.username" );
	public final static Property WORDPRESS_PASSWORD        			= config.new Property( "wordpress.password" );
	public final static Property WORDPRESS_URL          			= config.new Property( "wordpress.url" );
	public final static Property OLDPOSTS_ARCHIVE_FILENAME			= config.new Property( "oldpostarchive.filename" );
    public final static Property FTP_SERVER                         = config.new Property( "ftp.server");
    public final static Property FTP_USERNAME                       = config.new Property( "ftp.username");
    public final static Property FTP_PASSWORD                       = config.new Property( "ftp.password");
    public final static Property FTP_DIRECTORY                      = config.new Property( "ftp.directory");
    public final static Property HTTP_BASE_PATH                     = config.new Property( "http.base.path");
	public final static Property MODE								= config.new Property( "mode");
	public final static Property LOCAL_DIRECTORY					= config.new Property( "local.directory");

	protected Config(){
	}

	public static void init( URL... urls ) throws Exception {
		config.load( urls );
	}

	public static void unload(){
		config.reset();
	}

}
