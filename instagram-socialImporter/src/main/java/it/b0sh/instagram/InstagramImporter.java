package it.b0sh.instagram;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tearsofaunicorn.wordpress.api.WordpressClient;
import com.tearsofaunicorn.wordpress.api.WordpressClientConfig;
import com.tearsofaunicorn.wordpress.api.WordpressClientException;
import com.tearsofaunicorn.wordpress.api.model.Post;
import com.tearsofaunicorn.wordpress.api.transport.XmlRpcBridge;
import org.apache.commons.io.FileUtils;
import org.apache.commons.net.ftp.*;
import org.jinstagram.Instagram;
import org.jinstagram.auth.InstagramAuthService;
import org.jinstagram.auth.model.Token;
import org.jinstagram.auth.model.Verifier;
import org.jinstagram.auth.oauth.InstagramService;
import org.jinstagram.entity.common.ImageData;
import org.jinstagram.entity.common.Images;
import org.jinstagram.entity.users.feed.MediaFeed;
import org.jinstagram.entity.users.feed.MediaFeedData;
import org.jinstagram.entity.users.feed.UserFeed;
import org.jinstagram.entity.users.feed.UserFeedData;
import org.jinstagram.exceptions.InstagramException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Simone Galliani
 *         Date: 09/06/2015
 *         Time: 21:35
 *         Versioni:
 *         1.0 - 09/06/2015 - SG Creazione del file
 *         1.1 - 08/11/2015 - SG Due Modalita di pubblicazione delle immagini "locale" o "ftp"
 */
public class InstagramImporter {

    private static final Logger log = LoggerFactory.getLogger(InstagramImporter.class);

    private static final Token EMPTY_TOKEN = null;

    private List<String> oldPostIds;
    private List<String> newPostIds = new ArrayList<String>();


    public InstagramImporter(URL config) {
        try {
            Config.init(config);
        } catch (Exception e) {
            throw new RuntimeException("Configuration not  laoded", e);
        }
    }

    public void doImport(){

        Instagram instagram = new Instagram(Config.CLIENT_ID.value());

        UserFeed userFeed = null;

        Gson gson = new Gson();

        Type listPosts = new TypeToken<Collection<String>>() {}.getType();

        IMover fileMover;

        if ("local".equals(Config.MODE.value())){
            fileMover = new LocalMover(
                    Config.LOCAL_DIRECTORY.value());
        } else if ("ftp".equals(Config.MODE.value())) {
            fileMover = new FTPMover(
                    Config.FTP_SERVER.value(),
                    Config.FTP_USERNAME.value(),
                    Config.FTP_PASSWORD.value(),
                    Config.FTP_DIRECTORY.value());
        } else {
            throw new RuntimeException("MODE not supported");
        }





        try {



            WordpressClientConfig wcConfig;
            try {
                wcConfig = new WordpressClientConfig(Config.WORDPRESS_USERNAME.value(),
                        Config.WORDPRESS_PASSWORD.value(),
                        new URL(Config.WORDPRESS_URL.value()));
            } catch (MalformedURLException e) {
                throw new WordpressClientException("Failed to build Wordpress XML-RPC URL using configured property", e);
            }

            WordpressClient wpClient= new WordpressClient(new XmlRpcBridge(wcConfig));

            File oldPostFile = new File(Config.OLDPOSTS_ARCHIVE_FILENAME.value());
            if (oldPostFile.exists()) {
                oldPostIds = gson.fromJson(new FileReader(oldPostFile), listPosts);
            } else {
                oldPostIds = new ArrayList<String>();
            }



            userFeed = instagram.searchUser(Config.USERNAME.value());

            log.info("Found " + userFeed.getUserList().size() + " user");

            for (UserFeedData userdata : userFeed.getUserList()) {
                if (userdata.getUserName().equals(Config.USERNAME.value())) {
                    log.info("Found exact match");

                    MediaFeed mediaFeed = instagram.getRecentMediaFeed(userdata.getId());

                    log.info("Found " + mediaFeed.getData().size() + " recent medias");

                    for (MediaFeedData mfd : mediaFeed.getData()) {
                        if (!oldPostIds.contains(mfd.getId())) {
                            log.info("New media found ! id = " + mfd.getId());
                            newPostIds.add(mfd.getId());

                            Images images = mfd.getImages();
                            ImageData SDImage = images.getStandardResolution();
                            File imageFile = new File(mfd.getId() + ".jpg");
                            imageFile.delete();
                            FileUtils.copyURLToFile(new URL(SDImage.getImageUrl()), imageFile);

                            FileInputStream input = new FileInputStream(imageFile);

                            fileMover.move(imageFile,mfd.getId() + ".jpg");

                            imageFile.delete();

                            Post wpost = (new InstagramToWordpressProxy(mfd)).getPost();

                            log.debug(wpost.toString());
                            log.info("Created new post, id : " + wpClient.newPost(wpost));

                        }
                    }


                }

                oldPostIds.addAll(newPostIds);

                oldPostFile.delete();
                oldPostFile.createNewFile();

                FileWriter fw = new FileWriter(oldPostFile);

                gson.toJson(oldPostIds, fw);

                fw.flush();
                fw.close();


            }

        }catch (FTPConnectionClosedException e)   {
            System.err.println("FTP Server closed connection.");
            e.printStackTrace();

        } catch (InstagramException e) {
            log.error("Instagram Exception",e);
        } catch (IOException i){
            log.warn("Impossible to read data");
            throw new RuntimeException("Impossible to read data",i);
        }  finally {
            fileMover.detach();
        }



    }

}
